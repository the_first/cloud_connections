FROM python:3.6-stretch

COPY . /app

WORKDIR /app

RUN pip install --upgrade -r requirements.txt

CMD python connection/main.py
